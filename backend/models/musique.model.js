const mongoose = require('mongoose');

/**
 * This mongoDB schema is the validator of the music table
 * @type {module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, any>}
 */
const musicSchema = new mongoose.Schema(
    {
        filename: {
            type : String,
            required : true,
            trim : true,
            unique : true
        },

        titre : {
            type: String,
            required : true
        },

        auteur: {
            type : String
        },

        duree : {
            type : Number
        },

        url : {
            type : String,
            required : true
        },

        image : {
            type : String,
            required : true
        },

        bigImage : {
            type : String,
            required : true
        }
    },

    {
        timestamps: true
    }
);


const MusicModel = mongoose.model("musique", musicSchema);

module.exports = MusicModel;