const mongoose = require('mongoose');

/**
 * This mongoDB schema, is the validator of the playlist table
 * @type {module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, any>}
 */
const playlistSchema = new mongoose.Schema(
    {
        nom: {
            type : String,
            required : true,
            trim : true,
            unique : true
        },

        musiques : [{
            musique : {type : String},
            positionInPlaylist : {type : Number}
        }]
    },
    {
        timestamps: true
    }
);


const PlaylistModel = mongoose.model("playlist", playlistSchema);

module.exports = PlaylistModel;