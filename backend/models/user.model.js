const mongoose = require("mongoose");
const { encrypt, decrypt } = require("../utils/utils");

/**
 * This mongoDB schema, is the validator of the user table
 * @type {module:mongoose.Schema<any, Model<any, any, any, any>, {}, {}, any>}
 */
const userSchema = new mongoose.Schema(
  {
    pseudo: {
      type: String,
      required: true,
      unique: true,
      trim: true
    },

    password: {
      type: String,
      required: true,
      max: 1024,
      minlength: 6
    },

    likes: [
      {
        musique: { type: String },
        positionInLikes: { type: Number }
      }
    ],

    playlists: {
      type: [String]
    }
  },

  {
    timestamps: true
  }
);

// Fonction de cryptage du mdp avant sauvegarde dans la base de données
userSchema.pre("save", function (next) {
  const mdp = encrypt(this.password);
  this.password = mdp;
  next();
});

userSchema.statics.login = async function (pseudo, password) {
  const user = await this.findOne({ pseudo });
  if (user) {
    const userPassword = user.password;
    if (password === decrypt(userPassword)) {
      return user;
    } else {
      throw Error("incorrect password");
    }
  }
  throw Error("incorrect pseudo");
};

const UserModel = mongoose.model("user", userSchema);
module.exports = UserModel;
