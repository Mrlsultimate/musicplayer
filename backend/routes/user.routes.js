const router = require("express").Router();
const authController = require("../controllers/auth.controllers");
const User = require("../controllers/user.controllers");

const userController = new User();

/**
 * These are routes of the api that connect to the controllers
 */
router.post("/register", authController.signUp);
router.post("/login", authController.signIn);
router.get("/logout", authController.logout);

router.get("/getUser", userController.getUser);
router.put("/add-favoris/", userController.addFav);
router.put("/remove-favoris/", userController.removeFav);
router.put("/drag-drop/:id", userController.dragDrop);
router.post("/add-playlist/:id", userController.addPlaylist);

module.exports = router;
