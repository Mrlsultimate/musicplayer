const Playlist = require("../controllers/playlist.controllers");
const router = require("express").Router();
const playlistController = new Playlist();

/**
 * These are routes of the api that connect to the controllers
 */
router.post('/', playlistController.createPlaylist);
router.get('/', playlistController.getPlaylists);
router.get('/:id', playlistController.getPlaylist);
router.post('/add-music/:id', playlistController.addToPlaylist);
router.post('/remove-music/:id', playlistController.removeFromPlaylist);
router.post('/drag/:id', playlistController.dragDrop);

module.exports = router;