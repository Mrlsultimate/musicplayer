const router = require("express").Router();
const multer = require("multer");
const Music = require("../controllers/musique.controllers");
const allowedMimeTypes = ["audio/mp3"];
const musicController = new Music();

/**
 * This function filters the files received by the app to take only mp3
 * @param req the HTTP request
 * @param file the file
 * @param cb returns the file or null if it is not a mp3
 */
const filter = function (req, file, cb) {
  if (!allowedMimeTypes.includes(file.mimetype.toLowerCase())) {
    cb(null, false);
  }
  cb(null, true);
};

/**
 * This object is the config of the destination and new filename of the file
 * @type {DiskStorage}
 */
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./public/musiques/");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

/**
 * This function is a middleware that receive the file and filter it and then store it
 * @type {Multer}
 */
const upload = multer({ storage: storage, filter });

/**
 * These are routes of the api that connect to the controllers
 */
router.post("/upload", upload.single("file"), musicController.uploadMusic);
router.get("/music", musicController.getMusics);
router.get("/music/:id", musicController.getMusic);
router.get("/public/musiques/:nom", musicController.getMp3);

module.exports = router;
