const router = require("express").Router();
const root = "../frontend";

/**
 * These are routes that returns html files
 */
router.get("/login", (req, res) => {
  res.sendFile("./login.html", { root });
});

router.get("/accueil", (req, res) => {
  res.sendFile("./index.html", { root });
});

module.exports = router;
