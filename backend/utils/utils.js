const crypto = require("crypto");
const iv = Buffer.from(process.env.IV, "hex");
const key = Buffer.from(process.env.KEY, "utf-8");

/**
 * This function returns an encrypted text of the paramater
 * @param text the text to encrypt
 * @returns {string} the text encrypted
 */
module.exports.encrypt = (text) => {
  let cipher = crypto.createCipheriv("aes-256-cbc", Buffer.from(key), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  const mdp = encrypted.toString("hex");

  return mdp;
};

/**
 * This function decrypt the text
 * @param text the text to decrypt
 * @returns {string} the text decrypted
 */
module.exports.decrypt = (text) => {
  let encryptedText = Buffer.from(text, "hex");
  let decipher = crypto.createDecipheriv("aes-256-cbc", Buffer.from(key), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
};
