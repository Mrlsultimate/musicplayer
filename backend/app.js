/**
 * This script start the server on a specific port, it uses the configuration in server.js file
 */
const server = require("./server");
require("./config/db");

//server
server.listen(process.env.PORT);
console.log(`Listening on port ${process.env.PORT}`);
