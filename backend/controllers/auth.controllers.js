const UserModel = require("../models/user.model");
const jwt = require("jsonwebtoken");

const maxAge = process.env.TOKEN_MAXAGE;

/**
 * This function creates a token using an id and a secret token
 * @param id the id to hash
 * @returns {*} the token
 */
const createToken = (id) => {
  return jwt.sign({ id }, process.env.TOKEN_SECRET, {
    expiresIn: maxAge
  });
};

/**
 * This function allow a user to register an account using an HTTP request
 * @param req the HTTP request
 * @param res the response that will be send to the utilisateur
 * @returns {Promise<void>} the new user or an error
 */
module.exports.signUp = async (req, res) => {
  const { pseudo, password } = req.body;

  try {
    const user = await UserModel.create({ pseudo, password });

    res.status(201).json({ user: user._id });
  } catch (err) {
    res.status(400).send(err);
  }
};

/**
 * This function respond to the request login and check if the values are equals to an user in the database
 * @param req the HTTP request
 * @param res the response that will be send to the user
 * @returns {Promise<void>} a cookie with the user id hashed
 */
module.exports.signIn = async (req, res) => {
  const { pseudo, password } = req.body;

  try {
    const user = await UserModel.login(pseudo, password);
    const token = createToken(user._id);
    res.cookie("jwt", token, { httpOnly: true, maxAge });
    res.status(200).json({ user: user._id });
  } catch (err) {
    res.status(400).send(err);
  }
};

/**
 * This function allows user to logout
 * @param req the HTTP request
 * @param res the response that will be send to the user
 */
module.exports.logout = (req, res) => {
  res.cookie("jwt", "", { maxAge: 1 });
  res.redirect("/");
};
