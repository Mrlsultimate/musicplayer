const app = require("../server");
const request = require("supertest");
const mongoose = require("mongoose");
const PlaylistModel = require("../models/playlist.model");

describe("test playlist functions", () => {
  beforeAll(async () => {
    mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    const mockUser = {
      _id: "62b06de15474bb2d32fb5ee5",
      nom: "playlistTest",
      musiques: [
        {
          _id: "62b06de15474bb2d32fb5ee8",
          musique: "musiqueTest",
          positionInPlaylist: 1
        }
      ]
    };
    await PlaylistModel.deleteMany({});
    await PlaylistModel.create(mockUser);
  });

  afterAll(async () => {
    await PlaylistModel.deleteMany({});
    await mongoose.connection.close();
  });

  test("test getPlaylists", async () => {
    const response = await request(app)
      .get("/api/playlist")
      .set("Accept", "application/json")
      .expect(200);
    expect(response.body.length).toBe(1);
    expect(response.body[0]).toHaveProperty("_id", "62b06de15474bb2d32fb5ee5");
    expect(response.body[0]).toHaveProperty("nom", "playlistTest");
    expect(response.body[0]).toHaveProperty(
      "musiques[0].musique",
      "musiqueTest"
    );
    expect(response.body[0]).toHaveProperty(
      "musiques[0].positionInPlaylist",
      1
    );
  });

  test("test getPlaylist", async () => {
    const response = await request(app)
      .get("/api/playlist/62b06de15474bb2d32fb5ee5")
      .set("Accept", "application/json")
      .expect(200);
    expect(response.body).toHaveProperty("_id", "62b06de15474bb2d32fb5ee5");
    expect(response.body).toHaveProperty("nom", "playlistTest");
    expect(response.body).toHaveProperty("musiques[0].musique", "musiqueTest");
    expect(response.body).toHaveProperty("musiques[0].positionInPlaylist", 1);
  });

  test("test createPlaylist", async () => {
    const data = {
      nom: "newPlaylist",
      musiques: [
        {
          musique: "newMusique",
          positionInPlaylist: 42
        }
      ]
    };
    const response = await request(app)
      .post("/api/playlist")
      .send(data)
      .expect(200);
    expect(response.body).toHaveProperty("nom", "newPlaylist");
  });
  test("should not create a playlist whithout a name", async () => {
    const data = {
      musiques: [
        {
          musique: "newMusique",
          positionInPlaylist: 42
        }
      ]
    };
    await request(app).post("/api/playlist").send(data).expect(400);
  });

  test("should add a music into a playlist", async () => {
    const data = {
      _id: "15",
      musique: "hello",
      positionInPlaylist: 4
    };

    await request(app)
      .post("/api/playlist/add-music/62b06de15474bb2d32fb5ee5")
      .send(data)
      .expect(200);
  });

  test("should not add a music into a non-existant playlist ", async () => {
    const data = { _id: "11", musique: "boumboum", positionInPlaylist: 10 };

    await request(app)
      .post("/api/playlist/add-music/62b06de15474bb2d32fb5ee6")
      .send(data)
      .expect(400);
  });

  test("should remove a music from a playlist", async () => {
    const data = {
      id: "15"
    };

    await request(app)
      .post("/api/playlist/remove-music/62b06de15474bb2d32fb5ee5")
      .send(data)
      .expect(200);
  });

  test("should update the position of a music in a playlist", async () => {
    const data = {
      id: "musiqueTest",
      position: 11
    };

    const res = await request(app)
      .post("/api/playlist/drag/62b06de15474bb2d32fb5ee5")
      .send(data)
      .expect(200);

    expect(res.body.musiques[0]).toHaveProperty("positionInPlaylist", 11);
  });
});
