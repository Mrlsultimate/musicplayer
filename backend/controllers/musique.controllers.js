const MusicModel = require("../models/musique.model");

/**
 * This Music class is a controller for the music parts in the database
 * @type {Music}
 */
module.exports = class Music {
  MusicModel;

  /**
   * The constructor of the class Music that initialises some variables
   */
  constructor() {
    this.MusicModel = MusicModel;
  }

  /**
   * This function uploads a music in the database
   * @param req the HTTP request
   * @param res the response that will be send to the navigator
   * @returns {Promise<void>} the new document in the database or an error
   */
  async uploadMusic(req, res) {
    try {
      const { auteur, duree, image } = req.body;
      const url = "/public/musiques/" + req.file.filename;
      let titre = req.file.filename;
      titre = titre.replace("-", " ");
      titre = titre.replace(".mp3", "");
      titre = titre.replace("_", " ");
      const music = await MusicModel.create({
        filename: req.file.filename,
        auteur,
        duree,
        titre,
        url,
        image
      });
      res.status(200).send("file uploaded : " + music.id);
    } catch (err) {
      res.status(400).send(err);
    }
  }

  /**
   * This function returns all the musics in the database
   * @param req the HTTP request
   * @param res the response that will be send to the user
   * @returns {Promise<void>} the datas or an error
   */
  async getMusics(req, res) {
    try {
      const musics = await MusicModel.find().select(
        "_id titre duree auteur image bigImage url position"
      );
      res.status(200).json(musics);
    } catch (e) {
      res.status(400).send(e);
    }
  }

  /**
   * This function returns a music using the id send in the HTTP request
   * @param req the HTTP request
   * @param res the response that will be send to the user
   * @returns {Promise<void>} the data or an error
   */
  async getMusic(req, res) {
    try {
      const music = await MusicModel.findOne({ _id: req.params.id });
      res.status(200).json(music);
    } catch (e) {
      res.status(400).send(e);
    }
  }

  /**
   * This function returns the mp3 file of a music
   * @param req the HTTP request
   * @param res the response that will be send to the user
   * @returns {Promise<void>} the mp3
   */
  async getMp3(req, res) {
    res.sendFile("./public/musiques/" + req.params.nom.replace("%20", " "), {
      root: "./"
    });
  }
};
