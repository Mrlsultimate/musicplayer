const app = require("../server");
const request = require("supertest");
const mongoose = require("mongoose");
const MusicModel = require("../models/musique.model");

describe("test musique controller", () => {
  beforeAll(async () => {
    mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    const mockMusique = {
      _id: "62b1db04021e984a2263894a",
      image: "/pathImage",
      url: "/public/musiques/rick-roll.mp3",
      titre: "it's a title",
      bigImage: "/pathBigImage",
      filename: "filenameTest"
    };
    await MusicModel.deleteMany({});
    await MusicModel.create(mockMusique);
  });

  afterAll(async () => {
    await MusicModel.deleteMany({});
    await mongoose.connection.close();
  });

  test("test getMusics", async () => {
    const response = await request(app)
      .get("/api/musique/music")
      .set("Accept", "application/json")
      .expect(200);
    expect(response.body.length).toBe(1);
    expect(response.body[0]).toHaveProperty("image", "/pathImage");
    expect(response.body[0]).toHaveProperty(
      "url",
      "/public/musiques/rick-roll.mp3"
    );
    expect(response.body[0]).toHaveProperty("bigImage", "/pathBigImage");
    expect(response.body[0]).toHaveProperty("titre", "it's a title");
  });

  test("test getMusic", async () => {
    const response = await request(app)
      .get("/api/musique/music/62b1db04021e984a2263894a")
      .set("Accept", "application/json")
      .expect(200);
    expect(response.body).toHaveProperty("image", "/pathImage");
    expect(response.body).toHaveProperty(
      "url",
      "/public/musiques/rick-roll.mp3"
    );
    expect(response.body).toHaveProperty("bigImage", "/pathBigImage");
    expect(response.body).toHaveProperty("titre", "it's a title");
  });

  test("test getMp3", async () => {
    const response = await request(app)
      .get("/api/musique/public/musiques/rick-roll.mp3")
      .set("Accept", "application/json")
      .expect(200);
  });

  /*test("should return an error in uploadFile", async () => {
    const data = {
      _id: "1",
      image: "newpathImage",
      url: "newPathUrl",
      titre: "new title",
      bigImage: "newPathBigImage",
      filename: "newfilename"
    };
    await request(app).post("/api/musique/upload").send(data).expect(400);

    const response = await request(app)
      .get("/api/musique/music/1")
      .set("Accept", "application/json")
      .expect(400);
  });*/
});
