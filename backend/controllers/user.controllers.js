const UserModel = require("../models/user.model");
const jwt = require("jsonwebtoken");

/**
 * This function verify the token sent by a request
 * @param token the token to verify
 * @returns {*} returns the token verified
 */
const checkToken = (token) => {
  if (token) {
    return jwt.verify(token, process.env.TOKEN_SECRET);
  }
};

/**
 * This User class handles all the requests that asks to get or update things in the user table
 * @type {User}
 */
module.exports = class User {
  UserModel;
  jwt;

  /**
   * This constructor initialises the User class and models that are needed
   */
  constructor() {
    this.UserModel = UserModel;
    this.jwt = jwt;
  }

  /**
   * This function adds a music to the favorite playlist of a user
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<void>}
   */
  async addFav(req, res) {
    const idMusique = req.body.idMusique;

    const userCookie = req.cookies.jwt;
    if (userCookie) {
      const userToken = checkToken(userCookie);
      const users = await UserModel.find();
      users.map(async (user) => {
        if (user._id.toString() === userToken.id) {
          let likes = user.likes;
          try {
            const docs = await UserModel.findOneAndUpdate(
              { _id: user._id },
              {
                $addToSet: {
                  likes: {
                    musique: idMusique,
                    positionInLikes: likes.length + 1
                  }
                }
              },
              { new: true, upset: true, setDefaultsOnInsert: true }
            );
            res.status(200).send(docs);
          } catch (err) {
            res.status(400).send(err);
          }
        }
      });
    } else {
      res.status(400).send();
    }
  }

  /**
   * This function removes a music from the favorite playlist of an user
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<void>} returns the favorite playlist updated or an error
   */
  async removeFav(req, res) {
    const idMusique = req.body.idMusique;

    const userCookie = req.cookies.jwt;
    if (userCookie) {
      const userToken = checkToken(userCookie);
      const users = await UserModel.find();
      users.map(async (user) => {
        if (user._id.toString() === userToken.id) {
          try {
            const docs = await UserModel.findOneAndUpdate(
              { _id: user._id },
              {
                $pull: {
                  likes: {
                    musique: idMusique
                  }
                }
              },
              { new: true, upset: true, setDefaultsOnInsert: true }
            );
            res.status(200).send(docs);
          } catch (err) {
            res.status(400).send(err);
          }
        }
      });
    } else {
      res.status(400).send();
    }
  }

  /**
   * This function returns the info of the user without the password
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<void>} the user or an error
   */
  async getUser(req, res) {
    const userCookie = req.cookies.jwt;
    const userToken = checkToken(userCookie);
    try {
      const user = await UserModel.findOne({ _id: userToken.id }).select(
        "-password"
      );
      res.status(200).json(user);
    } catch (err) {
      res.status(400).send(err);
    }
  }

  /**
   * This function handles the drag and drop request and changes the position of musics inside the favorites of an user
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<void>} the favorites updated or an error
   */
  async dragDrop(req, res) {
    const userCookie = req.cookies.jwt;
    if (userCookie) {
      const userToken = checkToken(userCookie);
      const users = await UserModel.find();
      users.map(async (user) => {
        if (user._id.toString() === userToken) {
          user.likes.map((like) => {
            if (like.musique === req.params.id) {
              like.positionInLikes = req.body.position;
            }
          });
          try {
            const docs = await UserModel.findOneAndUpdate(
              { _id: user._id },
              { $set: { likes: user.likes } },
              {
                new: true,
                upset: true,
                setDefaultsOnInsert: true
              }
            );
            res.status(200).send(docs);
          } catch (err) {
            res.status(400).send(err);
          }
        }
      });
    } else {
      res.status(400).send("User introuvable");
    }
  }

  /**
   * This function adds a playlist to the list of playlist of the user
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<void>} the user updated or an error
   */
  async addPlaylist(req, res) {
    try {
      const docs = await UserModel.findOneAndUpdate(
        { _id: req.params.id },
        { $addToSet: { playlists: req.body.id } },
        {
          new: true,
          upset: true,
          setDefaultsOnInsert: true
        }
      );
      res.status(200).send(docs);
    } catch (err) {
      res.status(400).send(err);
    }
  }
};
