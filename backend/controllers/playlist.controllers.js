const PlaylistModel = require("../models/playlist.model");
const UserModel = require("../models/user.model");

/**
 * The class Playlist handles all the request that can be sent to the database about playlists
 * @type {Playlist}
 */
module.exports = class Playlist {
  PlaylistModel;
  UserModel;

  /**
   * This constructor of Playlist initialise the class with the models needed
   */
  constructor() {
    this.PlaylistModel = PlaylistModel;
    this.UserModel = UserModel;
  }

  /**
   * This function creates a Playlist in the database
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<void>} the new playlist or an error
   */
  async createPlaylist(req, res) {
    try {
      const docs = await PlaylistModel.create({ nom: req.body.nom });
      res.status(200).send(docs);
    } catch (err) {
      res.status(400).send(err);
    }
  }

  /**
   * This function returns all the playlists in the database
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<*>} returns the playlists or an error
   */
  async getPlaylists(req, res) {
    try {
      const playlists = await PlaylistModel.find();
      return res.status(200).send(playlists);
    } catch (err) {
      res.status(400).send(err);
    }
  }

  /**
   * This function returns a playlist
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<*>} returns the playlist or an error
   */
  async getPlaylist(req, res) {
    try {
      const playlist = await PlaylistModel.findOne({ _id: req.params.id });
      return res.status(200).send(playlist);
    } catch (err) {
      res.status(400).send(err);
    }
  }

  /**
   * This function adds a music to a playlist
   * @param req The HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<void>} returns the playlist updated or an error
   */
  async addToPlaylist(req, res) {
    const id = req.params.id;
    const idMusique = req.body.id;
    try {
      const playlist = await PlaylistModel.findOne({ _id: id });
      const docs = await PlaylistModel.findOneAndUpdate(
        { _id: id },
        {
          $addToSet: {
            musiques: {
              musique: idMusique,
              positionInPlaylist: playlist.musiques.length + 1
            }
          }
        },
        {
          new: true,
          upset: true,
          setDefaultsOnInsert: true
        }
      );
      res.status(200).send(docs);
    } catch (err) {
      res.status(400).send(err);
    }
  }

  /**
   * This function removes a music from the playlist
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<void>} returns the playlist updated or an error
   */
  async removeFromPlaylist(req, res) {
    const id = req.params.id;
    const idMusique = req.body.id;
    try {
      const docs = await PlaylistModel.findOneAndUpdate(
        { _id: id },
        {
          $pull: {
            musiques: {
              musique: idMusique
            }
          }
        },
        {
          new: true,
          upset: true,
          setDefaultsOnInsert: true
        }
      );
      res.status(200).send(docs);
    } catch (err) {
      res.status(400).send(err);
    }
  }

  /**
   * This function handles the drag and drop in the playlist and changes position of musics
   * @param req the HTTP request
   * @param res the response that will be sent to the user
   * @returns {Promise<void>} returns the playlist updated or an error
   */
  async dragDrop(req, res) {
    const playlists = await PlaylistModel.find();
    playlists.map(async (playlist) => {
      if (playlist._id.toString() === req.params.id) {
        playlist.musiques.map((musique) => {
          if (musique.musique === req.body.id) {
            musique.positionInPlaylist = req.body.position;
          }
        });
        try {
          const docs = await PlaylistModel.findOneAndUpdate(
            { _id: req.params.id },
            { $set: { musiques: playlist.musiques } },
            {
              new: true,
              upset: true,
              setDefaultsOnInsert: true
            }
          );
          res.status(200).send(docs);
        } catch (err) {
          res.status(400).send(err);
        }
      }
    });
  }
};
