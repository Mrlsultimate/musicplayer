const app = require("../server");
const request = require("supertest");
const mongoose = require("mongoose");
const UserModel = require("../models/user.model");

describe("test authentification functions", () => {
  beforeAll(async () => {
    mongoose.connect(process.env.DB_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    const mockUser = {
      pseudo: "pseudoTest",
      password: "passwordTest"
    };
    await UserModel.deleteMany({});
    await UserModel.create(mockUser);
  });
  afterAll(async () => {
    await UserModel.deleteMany({});
    await mongoose.connection.close();
  });
  test("test signIn", async () => {
    const data = {
      pseudo: "pseudoTest",
      password: "passwordTest"
    };
    const response = await request(app)
      .post("/api/user/login")
      .send(data)
      .expect(200);
    expect(response.body).toHaveProperty("user");
  });

  test("should not work beacause user doesn't exist, signIn", async () => {
    const data = {
      pseudo: "abcdef",
      password: "abcdef"
    };
    await request(app).post("/api/user/login").send(data).expect(400);
  });

  test("test signUp", async () => {
    const data = {
      pseudo: "newPseudo",
      password: "newPassword"
    };
    const response = await request(app)
      .post("/api/user/register")
      .send(data)
      .expect(201);
    expect(response.body).toHaveProperty("user");
  });

  test("should not signUp becuse missing information", async () => {
    const data = {
      pseudo: "newPseudo"
    };
    await request(app).post("/api/user/register").send(data).expect(400);
  });

  test("should redirect when logout", async () => {
    await request(app).get("/api/user/logout").expect(302);
  });
});
