/*const app = require("../server");
const request = require("supertest");
const { MongoClient } = require("mongodb");

describe("test user functions", () => {
  let jwt;
  let client;
  let connection;
  let db;
  let collection;
  let users;

  beforeAll(async () => {
    // Build the connection String and create the database connection
    client = new MongoClient(process.env.DB_URL);
    try {
      await client.connect();
      const database = client.db("musiquetest");
      users = database.collection("users");
      const mockUser = {
        pseudo: "pseudoTest",
        password: "passwordTest"
      };
      const res = await users.insertOne(mockUser);
      const response = await request(app)
        .post("/api/user/login")
        .set("Accept", "application/json")
        .send({ pseudo: "pseudoTest", password: "passwordTest" });
      jwt = response.header["set-cookie"][0].split(";")[0];
    } catch (err) {
      return console.log("Database Connection Error!", err.message);
    }
  });

  afterAll(async () => {
    //await users.deleteMany({});
    await client.close();
  });

  test("should not get User with a wrong token", async () => {
    const jwt = "azerty-1234567890";
    const response = await request(app)
      .get("/api/user/getUser")
      .set("Accept", "application/json")
      .set("Cookie", `jwt=${jwt}`)
      .expect(400);
  });

  test("test getUser", async () => {
    await request(app)
      .get("/api/user/getUser")
      .set("Accept", "application/json")
      .set("Cookie", `${jwt}`)
      .expect(200);
    expect(true);
  });
});
*/
