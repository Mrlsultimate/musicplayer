const mongoose = require("mongoose");
try {
  mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  console.log("Connected to MongoDB");
} catch (err) {
  console.log("Failed to connect to MongoDB", err);
}
