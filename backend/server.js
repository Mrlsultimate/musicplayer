/**
 * This script setups the server Express with cors, cookie-parser, json for express, routes and connection to the database
 * @type {e | (() => Express)}
 */

if (process.env.NODE_ENV.trim() === "development") {
  require("dotenv").config({ path: "./config/.env" });
}
if (process.env.NODE_ENV.trim() == "test")
  require("dotenv").config({ path: "./config/.env.test" });

const express = require("express");
const server = express();
const cors = require("cors");
const cookieParser = require("cookie-parser");

const routes = require("./routes/routes");
const userRoutes = require("./routes/user.routes");
const musicRoutes = require("./routes/musique.routes");
const playlistRoutes = require("./routes/playlist.routes");

//json parser
server.use(express.json());

server.use(express.static("../frontend/"));

//cors
const corsOptions = {
  origin: "http://localhost:3000",
  credentials: true,
  allowedHeaders: ["sessionId", "Content-Type"],
  exposedHeaders: ["sessionId"],
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  preflightContinue: false
};

server.use(cors(corsOptions));

server.use(cookieParser());

server.use("/", routes);
server.use("/api/user", userRoutes);
server.use("/api/musique", musicRoutes);
server.use("/api/playlist", playlistRoutes);

module.exports = server;
