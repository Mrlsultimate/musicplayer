import { Utils } from "./utils/utils.js";

export class Footer {

  /**
   * The html button used to go back to the last track
   */
  prev_btn;

  /**
   * The html button used to pause the music
   */
  playpause_btn;

  /**
   * The html button used to go to the next music
   */
  next_btn;

  /**
   * The html element which gives the information of the music timer
   */
  curr_time;

  /**
   * The html element which gives the information of the music duration
   */
  total_duration;

  /**
   * The html slider representing the music timer
   */
  seek_slider;

  /**
   * The html button used to shuffle
   */
  shuffle_btn;

  /**
   * The html button used to repeat the current music
   */
  repeat_btn;

  /**
   * The html audio element, which permits to play the music
   */
  curr_track;

  /**
   * The html button which permits to show or hide the musics list
   */
  buttonTogglePlaylist;

  /**
   * The constructor of the footer, which initializes its attributes. Here the html elements of the footer are created
   * @param nextTrack the function which permits to go to next track
   * @param playPauseMusic the function which is triggered when the user click on the play/pause button
   * @param prevTrack the function which permits to go to last track
   * @param shuffle the function which permits to switch the shuffle state
   * @param repeat the function which permits to switch the repeat state
   * @param collapsePlaylist the function which permits to hide or show the musics
   */
  constructor(nextTrack, playPauseMusic, prevTrack, shuffle, repeat, collapsePlaylist) {
    this.prev_btn = document.querySelector(".prev");
    this.playpause_btn = document.getElementById("play-pauseButton");
    this.next_btn = document.querySelector(".next");
    this.curr_time = document.querySelector(".en-cours");
    this.total_duration = document.querySelector(".total");
    this.seek_slider = document.querySelector(".slider");

    this.shuffle_btn = document.querySelector(".shuffle");
    this.repeat_btn = document.querySelector(".repeat");
    this.curr_track = document.createElement("audio");
    this.buttonTogglePlaylist = document.querySelector(".toggle-playlist");
    this.buttonTogglePlaylist.addEventListener("click", collapsePlaylist);

    this.curr_track.addEventListener("ended", nextTrack);
    this.playpause_btn.addEventListener("click", playPauseMusic);
    this.seek_slider.oninput = () => {
      this.seekTo(this.seek_slider.value);
    };
    this.prev_btn.addEventListener("click", prevTrack);
    this.next_btn.addEventListener("click", nextTrack);
    this.shuffle_btn.addEventListener("click", shuffle);
    this.repeat_btn.addEventListener("click", repeat);
  }

  /**
   * This functions reset the footer values (duration, timer etc.)
   */
  resetValues = () => {
    this.curr_time.textContent = "00:00";
    this.total_duration.textContent = "00:00";
    this.seek_slider.value = 0;
  }

  /**
   * This function permits to update the slider value, during the music
   */
  seekUpdate = () => {
    if (!isNaN(this.curr_track.duration)) {
      this.seek_slider.value = (this.curr_track.currentTime * 100) / this.curr_track.duration;
      this.curr_time.textContent = Utils.secondToMinutes(this.curr_track.currentTime);
      this.total_duration.textContent = Utils.secondToMinutes(this.curr_track.duration);
    }
  }

  /**
   * This function permits to forward in the music
   */
  seekTo = (value) => {
    this.curr_track.currentTime = (this.curr_track.duration * value) / 100;
  }
}