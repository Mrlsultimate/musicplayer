/**
 * The login class which handle the user connection
 */
export class Login {

    static login = async (pseudo, password) => {
        return fetch("http://localhost:3000/api/user/login", {
            method: "post",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                pseudo: pseudo,
                password: password
            })
        }).then(async (response) => {
            try {
                const data = await response.json();
                if (data.user) {
                    window.location = "/accueil";
                }else{
                    alert("Identifiants incorrects");
                }
            } catch (err) {
                console.log(err);
            }
        });
    }
}

document.getElementById("connect-button").addEventListener(('click'), () => {
    Login.login(document.getElementById("inputPseudo").value, document.getElementById("inputPassword").value);
});