import { Utils } from "./utils/utils.js";
import { Footer } from "./footer.js";
import { Like } from "./like.js";

/**
 * This class represents the queue of musics
 */
export class Queue {
  /**
   * The index of the current music
   */
  trackIndex;

  /**
   * The footer of the application which needs to be update with the queue
   */
  footer;

  /**
   * The boolean which is true if a music is playing, else false
   */
  isPlaying;

  /**
   * Using to update the timer, and the duration slider of a music
   */
  updateTimer;

  /**
   * Boolean true if the button shuffle has been clicked once, false if it has been clicked twice
   */
  isShuffleOn;

  /**
   * Boolean true if the repeat shuffle has been clicked once, false if it has been clicked twice
   */
  isRepeatOn;

  /**
   * The music list of the queue
   */
  listMusic;

  /**
   * The list of html elements representing the lines in the queue (one item is the html division of one music)
   */
  listItem;

  dragSrcEl;

  /**
   * constructor of the queue, which initializes its parameters.
   * @param collapsePlaylist, the function which is pass to the footer, triggered to the click on a footer button
   */
  constructor(collapsePlaylist) {
    this.trackIndex = 0;
    this.footer = new Footer(
      this.nextTrack,
      this.playPauseMusic,
      this.prevTrack,
      this.shuffle,
      this.repeat,
      collapsePlaylist
    );
    this.listItem = document.getElementsByClassName("item-list");
    this.isPlaying = false;
    this.isShuffleOn = false;
    this.isRepeatOn = false;
    document.querySelectorAll(".likeCurrentMusic").forEach((img) => {
      img.parentElement.addEventListener("click", (e) =>
        Like.setToFav(e.target.parentElement)
      );
    });
  }

  /**
   * It initializes the listMusic attribute, by a request to the backend. Initializes also the footer and the left panel with
   * informations of the first track
   * @returns {Promise<void>}
   */
  init = async () => {
    Utils.getListeMusique().then((data) => {
      this.listMusic = data;
    });
    await this.prepareFirstTrack();
  };

  /**
   * Setter for the trackIndex
   * @param newIndex the new value of trackIndex
   */
  setTrackIndex = (newIndex) => {
    this.trackIndex = newIndex;
  };

  /**
   * Function call to play a music
   * @param url, the url of the music to play
   */
  playNewMusic = (url) => {
    this.loadTrack(url);
    this.playTrack();
  };

  /**
   * This function permits to load the music on the html media element
   * @param url, the url of the music to load
   */
  loadTrack = (url) => {
    clearInterval(this.updateTimer);
    this.footer.resetValues();
    this.footer.curr_track.src = url;

    this.footer.curr_track.load();
    this.updateTimer = setInterval(this.footer.seekUpdate, 100);
  };

  /**
   * This functions set the trackindex in function of the options checked in the footer (shuffle, repeat etc.)
   * @returns {Promise<void>}
   */
  nextTrack = async () => {
    if (this.isShuffleOn) {
      this.setTrackIndex(Math.floor(Math.random() * this.listMusic.length));
    } else if (!this.isRepeatOn) {
      this.setTrackIndex(this.trackIndex + 1);
    }
    if (this.trackIndex === this.listMusic.length && !this.isRepeatOn) {
      this.setTrackIndex(0);
    }
    this.playNewMusic("api/musique" + this.listMusic[this.trackIndex].url);
    let isFav = await Utils.isFavoriteMusic(this.listMusic[this.trackIndex]);
    this.updateLecture(
      this.listItem[this.trackIndex],
      this.listMusic[this.trackIndex],
      isFav
    );
  };

  /**
   * Call when the user click on pause button. If a music is playing, setting it on pause, else, resume it
   */
  playPauseMusic = () => {
    if (!this.isPlaying) this.playTrack();
    else this.pauseTrack();
  };

  /**
   * This function is call to play the music which is loaded.
   */
  playTrack = () => {
    this.footer.curr_track.play();
    this.isPlaying = true;
    this.footer.playpause_btn.style.backgroundImage =
      "url('assets/images/pause.png')";
  };

  /**
   * Set the music playing, in pause.
   */
  pauseTrack = () => {
    this.footer.curr_track.pause();
    this.isPlaying = false;
    this.footer.playpause_btn.style.backgroundImage =
      "url('assets/images/play.svg')";
  };

  /**
   * Return to the previous track
   * @returns {Promise<void>}
   */
  prevTrack = async () => {
    if (this.trackIndex === 0 && !this.isRepeatOn) {
      this.setTrackIndex(this.listMusic.length - 1);
    } else if (!this.isRepeatOn) {
      this.setTrackIndex(this.trackIndex - 1);
    }
    this.playNewMusic("api/musique" + this.listMusic[this.trackIndex].url);
    let isFav = await Utils.isFavoriteMusic(this.listMusic[this.trackIndex]);
    this.updateLecture(
      this.listItem[this.trackIndex],
      this.listMusic[this.trackIndex],
      isFav
    );
  };

  /**
   * This function is called when the shuffle button is clicked. Permits to change the design of the button
   */
  shuffle = () => {
    this.isShuffleOn = !this.isShuffleOn;
    if (this.isShuffleOn && this.isRepeatOn) {
      this.footer.shuffle_btn.style.backgroundImage =
        "url('assets/images/shuffleOn.png')";
      this.repeat();
    } else if (this.isShuffleOn) {
      this.footer.shuffle_btn.style.backgroundImage =
        "url('assets/images/shuffleOn.png')";
    } else {
      this.footer.shuffle_btn.style.backgroundImage =
        "url('assets/images/shuffle.png')";
    }
  };

  /**
   * This function is called when the repeat button is clicked. Permits to change the design of the button
   */
  repeat = () => {
    this.isRepeatOn = !this.isRepeatOn;
    if (this.isRepeatOn && this.isShuffleOn) {
      this.footer.repeat_btn.style.backgroundImage =
        "url('assets/images/repeatOn.png')";
      this.shuffle();
    } else if (this.isRepeatOn) {
      this.footer.repeat_btn.style.backgroundImage =
        "url('assets/images/repeatOn.png')";
    } else {
      this.footer.repeat_btn.style.backgroundImage =
        "url('assets/images/repeat.png')";
    }
  };

  /**
   * Loading the first track of the listMusic (choose by its index). It also check if this music is in favorite.
   * @returns {Promise<void>}
   */
  prepareFirstTrack = async () => {
    this.loadTrack("api/musique" + this.listMusic[this.trackIndex].url);

    document.getElementById("currentMusicCover").src =
      this.listMusic[this.trackIndex].image;
    document.getElementById("coverFooter").src =
      this.listMusic[this.trackIndex].image;
    document.querySelector(".titre").textContent =
      this.listMusic[this.trackIndex].titre;
    document.querySelector(".artiste").textContent =
      this.listMusic[this.trackIndex].auteur;

    let listeMusiqueBDD;
    Utils.getListeMusique().then((data) => {
      listeMusiqueBDD = data;
    });
    await this.iterateListeItem(listeMusiqueBDD);
    let isFav = await Utils.isFavoriteMusic(this.listMusic[this.trackIndex]);
    this.updateLecture(
      this.listItem[this.trackIndex],
      this.listMusic[this.trackIndex],
      isFav
    );
  };

  /**
   * This function design the music playing
   * @param divItem the html div relating to the music playing
   * @param music the music which will be play
   * @param isFav true if the music is in the favorites of the user
   */
  updateLecture = (divItem, music, isFav) => {
    Array.from(this.listItem).forEach((element) => {
      if (element.classList.contains("active-music"))
        element.classList.remove("active-music");
    });
    divItem.classList.add("active-music");
    document.getElementById("musicTitle").innerHTML = music.titre;
    document.getElementById("singer").innerHTML = music.auteur;
    document.querySelector(".titre").textContent = music.titre;
    document.querySelector(".artiste").textContent = music.auteur;
    document.querySelector("#currentMusicCover").src = music.bigImage;
    document.querySelector("#coverFooter").src = music.image;
    document.querySelectorAll(".likeCurrentMusic").forEach((heart) => {
      heart.dataset.id = music._id;
      if (isFav) {
        heart.src = "assets/images/heart-filled.svg";
      } else {
        heart.src = "assets/images/heart.svg";
      }
    });
  };

  /**
   * This method display the list of musics
   * @returns {Promise<void>}
   */
  iterateListeItem = async (listeMusiqueBDD) => {
    let arrayOfFav;

    let response = await Utils.getUser();
    arrayOfFav = response.likes;

    let divListe = document.getElementById("liste");
    listeMusiqueBDD.forEach((element, index) => {
      let isFav = false;

      if (arrayOfFav) {
        for (let i = 0; i < arrayOfFav.length; i++) {
          if (arrayOfFav[i].musique === element._id) {
            isFav = true;
            break;
          }
        }
      }

      let divItem = document.createElement("div");
      divItem.className = "item-list";

      divItem.setAttribute("draggable", "true");
      divItem.addEventListener("dragstart", (e) => {
        this.handleDragStart(e, divItem);
      });
      divItem.addEventListener("dragend", (e) =>
        this.handleDragEnd(e, divItem)
      );
      divItem.addEventListener("dragover", this.handleDragOver);
      divItem.addEventListener("dragenter", (e) => {
        this.handleDragEnter(e, divItem);
      });
      divItem.addEventListener("dragleave", (e) =>
        this.handleDragLeave(e, divItem)
      );
      divItem.addEventListener("drop", (e) => this.handleDrop(e, divItem));

      let divItemLeft = document.createElement("div");
      divItemLeft.className = "item-list-left";
      let divItemCenter = document.createElement("div");
      divItemCenter.className = "item-list-center";
      let divItemRight = document.createElement("div");
      divItemRight.className = "item-list-right";

      let divLogo = document.createElement("div");
      divLogo.className = "item-jacket-icon";
      divLogo.style.backgroundImage = "url(" + element.image + ")";
      divLogo.addEventListener("click", async () => {
        this.playNewMusic("api/musique" + element.url);
        this.setTrackIndex(index);
        let isFav = await Utils.isFavoriteMusic(
          this.listMusic[this.trackIndex]
        );
        this.updateLecture(
          this.listItem[this.trackIndex],
          this.listMusic[this.trackIndex],
          isFav
        );
      });
      let buttonLike = document.createElement("button");
      buttonLike.className = "item-button-like";
      let imgOfButtonLike = document.createElement("img");
      if (isFav) {
        imgOfButtonLike.src = "assets/images/heart-filled.svg";
      } else {
        imgOfButtonLike.src = "assets/images/heart.svg";
      }
      imgOfButtonLike.dataset.id = element._id;
      buttonLike.addEventListener("click", (e) =>
        Like.setToFav(e.target.parentElement)
      );
      let divTitle = document.createElement("div");
      divTitle.className = "item-title";
      divTitle.style.flexGrow = "1";
      divTitle.style.cursor = "pointer";
      divTitle.innerHTML = element.titre;
      divTitle.onclick = async () => {
        this.playNewMusic("api/musique" + element.url);
        this.setTrackIndex(index);
        let isFav = await Utils.isFavoriteMusic(
          this.listMusic[this.trackIndex]
        );
        this.updateLecture(
          this.listItem[this.trackIndex],
          this.listMusic[this.trackIndex],
          isFav
        );
      };

      let divMenu = document.createElement("div");
      divMenu.className = "item-menu invisible";
      let divMenuFirstLine = document.createElement("div");
      divMenuFirstLine.className = "menu-line";
      let divMenuFirstLineFirstElement = document.createElement("div");
      let divMenuFirstLineSecondElement = document.createElement("div");
      divMenuFirstLineFirstElement.className =
        "first-element-of-line-in-menu first-line";
      divMenuFirstLineSecondElement.innerHTML = "Ecouter juste après";
      divMenuFirstLine.appendChild(divMenuFirstLineFirstElement);
      divMenuFirstLine.appendChild(divMenuFirstLineSecondElement);

      let divMenuSecondLine = document.createElement("div");
      divMenuSecondLine.className = "menu-line";
      let divMenuSecondLineFirstElement = document.createElement("div");
      let divMenuSecondLineSecondElement = document.createElement("div");
      divMenuSecondLineFirstElement.className =
        "first-element-of-line-in-menu second-line";
      divMenuSecondLineSecondElement.innerHTML = "Ajouter à la file d'attente";
      divMenuSecondLine.appendChild(divMenuSecondLineFirstElement);
      divMenuSecondLine.appendChild(divMenuSecondLineSecondElement);

      let divMenuThirdLine = document.createElement("div");
      divMenuThirdLine.className = "menu-line";
      let divMenuThirdLineFirstElement = document.createElement("div");
      let divMenuThirdLineSecondElement = document.createElement("div");
      divMenuThirdLineFirstElement.className =
        "first-element-of-line-in-menu third-line";
      divMenuThirdLineSecondElement.innerHTML = "Ajouter aux favoris";
      divMenuThirdLine.appendChild(divMenuThirdLineFirstElement);
      divMenuThirdLine.appendChild(divMenuThirdLineSecondElement);

      let divMenuFourthLine = document.createElement("div");
      divMenuFourthLine.className = "menu-line";
      let divMenuFourthLineFirstElement = document.createElement("div");
      let divMenuFourthLineSecondElement = document.createElement("div");
      divMenuFourthLineFirstElement.className =
        "first-element-of-line-in-menu fourth-line";
      divMenuFourthLineSecondElement.innerHTML = "Ne pas me recommander";
      divMenuFourthLine.appendChild(divMenuFourthLineFirstElement);
      divMenuFourthLine.appendChild(divMenuFourthLineSecondElement);

      let divMenuFiveLine = document.createElement("div");
      divMenuFiveLine.className = "menu-line";
      let divMenuFiveLineFirstElement = document.createElement("div");
      let divMenuFiveLineSecondElement = document.createElement("div");
      divMenuFiveLineFirstElement.className =
        "first-element-of-line-in-menu five-line";
      divMenuFiveLineSecondElement.innerHTML = "Ajouter à une playlist";
      divMenuFiveLine.appendChild(divMenuFiveLineFirstElement);
      divMenuFiveLine.appendChild(divMenuFiveLineSecondElement);

      let divMenuSixthLine = document.createElement("div");
      divMenuSixthLine.className = "menu-line";
      let divMenuSixthLineFirstElement = document.createElement("div");
      let divMenuSixthLineSecondElement = document.createElement("div");
      divMenuSixthLineFirstElement.className =
        "first-element-of-line-in-menu sixth-line";
      divMenuSixthLineSecondElement.innerHTML = "Mixes inspirés par ce titre";
      divMenuSixthLine.appendChild(divMenuSixthLineFirstElement);
      divMenuSixthLine.appendChild(divMenuSixthLineSecondElement);

      let divDots = document.createElement("div");
      divDots.className = "item-dots";
      divDots.addEventListener("click", () => {
        if (divMenu.classList.contains("visible")) {
          divMenu.classList.remove("visible");
          divMenu.classList.add("invisible");
        } else if (divMenu.classList.contains("invisible")) {
          divMenu.classList.remove("invisible");
          divMenu.classList.add("visible");
        }
      });
      divMenu.addEventListener("mouseleave", () => {
        if (divMenu.classList.contains("visible")) {
          divMenu.classList.remove("visible");
          divMenu.classList.add("invisible");
        } else if (divMenu.classList.contains("invisible")) {
          divMenu.classList.remove("invisible");
          divMenu.classList.add("visible");
        }
      });
      let divLyrics = document.createElement("div");
      divLyrics.className = "item-lyrics";
      let divSinger = document.createElement("div");
      divSinger.className = "item-singer";
      divSinger.innerHTML = element.auteur;

      let divTime = document.createElement("div");
      divTime.className = "item-time";
      divTime.innerHTML = Utils.secondToMinutes(element.duree);
      divItem.id = element._id;

      divItemLeft.appendChild(divLogo);
      divItemLeft.appendChild(buttonLike);
      divItemLeft.appendChild(divTitle);
      buttonLike.appendChild(imgOfButtonLike);

      divItemCenter.appendChild(divDots);
      divItemCenter.appendChild(divLyrics);
      divItemCenter.appendChild(divSinger);

      divItemRight.appendChild(divTime);

      divItem.appendChild(divItemLeft);
      divItem.appendChild(divMenu);
      divItem.appendChild(divItemCenter);
      divItem.appendChild(divItemRight);

      divMenu.appendChild(divMenuFirstLine);
      divMenu.appendChild(divMenuSecondLine);
      divMenu.appendChild(divMenuThirdLine);
      divMenu.appendChild(divMenuFourthLine);
      divMenu.appendChild(divMenuFiveLine);
      divMenu.appendChild(divMenuSixthLine);

      divListe.appendChild(divItem);
    });
  };

    /**
     * This function is used when a div is cliked to be drag. It changes its style.
     * @param e the event dragstart
     * @param div the division which is drag by the user
     */
  handleDragStart = (e, div) => {
    div.style.opacity = "0.4";

    this.dragSrcEl = div;

    e.dataTransfer.effectAllowed = "move";
    e.dataTransfer.setData("text/html", div.innerHTML);
  };

    /**
     * This function permits to receive the dragend event out of a dropzone
     * @param e the event dragend
     * @param div the div which is dragged
     */
  handleDragEnd = (e, div) => {
    div.style.opacity = "1";
  };

    /**
     * This function permits to cancel the dragover event
     * @param e the event dragover
     * @returns {boolean} false
     */
  handleDragOver = (e) => {
    e.preventDefault();
    return false;
  };

    /**
     * This function permits modify the design when the mouse hover a dropzone
     * @param e the event dragenter
     * @param div the division of the dropzone
     */
  handleDragEnter = (e, div) => {
    div.classList.add("over");
  };


    /**
     * This function permits modify the design when the mouse leave a dropzone
     * @param e the event dragleave
     * @param div the division of the dropzone
     */
  handleDragLeave = (e, div) => {
    div.classList.remove("over");
  };

    /**
     * This functions handle the event drop, when the dragged division is dropped
     * @param e the event drop
     * @param div the division which is dropped
     * @returns {boolean} false
     */
  handleDrop = (e, div) => {
    e.stopPropagation();
    div.classList.remove("over");
    if (this.dragSrcEl !== div) {
      let list = document.getElementById("liste");
      let newDiv = document.createElement("div");
      newDiv.classList.add("item-list");
      newDiv.innerHTML = e.dataTransfer.getData("text/html");
      newDiv.id = this.dragSrcEl.id;

      newDiv.setAttribute("draggable", "true");
      newDiv.addEventListener("dragstart", (e) => {
        this.handleDragStart(e, newDiv);
      });
      newDiv.addEventListener("dragend", (e) => this.handleDragEnd(e, newDiv));
      newDiv.addEventListener("dragover", this.handleDragOver);
      newDiv.addEventListener("dragenter", (e) => {
        this.handleDragEnter(e, newDiv);
      });
      newDiv.addEventListener("dragleave", (e) =>
        this.handleDragLeave(e, newDiv)
      );
      newDiv.addEventListener("drop", (e) => this.handleDrop(e, newDiv));

      list.insertBefore(newDiv, div);
      this.dragSrcEl.remove();
      this.updatePlaylist(newDiv.id);
    }
    return false;
  };

    /**
     * This function is called to update playlist in the api after a drag and drop
     * @param id the id of the music which has been moved
     */
  updatePlaylist = (id) => {
    let list = document.getElementsByClassName("item-list");
    Array.from(list).forEach((item, index) => {
      if (id === item.id) this.eventListener(item, index);
      fetch("/api/user/drag-drop/:id", {
        method: "put",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          id: item.id,
          position: index
        })
      });
    });
  };

    /**
     * This function permits to add all the listeners on html division after the drag and drop
     * @param div the division which we set the listeners
     * @param index the index of this division
     */
  eventListener = (div, index) => {
    Utils.getOneMusic(div.id, (res) => {
      let music = res;
      let isFav =
        div.querySelector("[data-id='" + music._id + "']").src ===
        "http://localhost:3000/assets/images/heart-filled.svg";
      div
        .getElementsByClassName("item-jacket-icon")[0]
        .addEventListener("click", () => {
          this.playNewMusic("api/musique" + music.url);
          this.updateLecture(div, music, isFav);
          this.setTrackIndex(index);
        });

      div.getElementsByClassName("item-title")[0].onclick = () => {
        this.playNewMusic("api/musique" + music.url);
        this.updateLecture(div, music, isFav);
        this.setTrackIndex(index);
      };
    });
  };
}
