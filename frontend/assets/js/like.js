/**
 * This class is used to set a music to the favorites
 */
export class Like {

    /**
     * This function make a request to backend to set a music to favorites
     * @param element the element clicked by the user, used to re design the heart button
     */
    static setToFav = (element) => {
        let img = element.querySelector("img");
        let musicId = img.getAttribute("data-id");
        if (musicId){
            document.querySelectorAll('[data-id = "' + musicId + '"]').forEach((img) => {
                if (img.src === "http://localhost:3000/assets/images/heart-filled.svg") {
                    fetch("/api/user/remove-favoris/", {
                        method: "put",
                        credentials : "include",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            idMusique : musicId
                        })
                    }).then ((res) => {
                        if (res.status === 200){
                            img.src = "http://localhost:3000/assets/images/heart.svg";
                        }
                    });
                } else {
                    fetch("/api/user/add-favoris/", {
                        method: "put",
                        credentials : "include",
                        headers: {
                            Accept: "application/json",
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            idMusique : musicId
                        })
                    }).then ((res) => {
                        if (res.status === 200){
                            img.src = "http://localhost:3000/assets/images/heart-filled.svg";
                        }
                    });
                }
            });
        }
    }
}
