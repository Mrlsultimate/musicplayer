import { Playlist } from "./playlist.js";

const playlist = new Playlist();
playlist.init();
playlist.queue.init();
