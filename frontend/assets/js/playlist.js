import { Utils } from "./utils/utils.js";
import { Queue } from "./queue.js";

/**
 * This class represents a playlist, which contains the queue
 */
export class Playlist {
  /**
   * The queue of music
   */
  queue;

  /**
   * The html division playlist
   */
  playlist;

  /**
   * The html button to close the playlist
   */
  closeButton;

  /**
   * The html dropdown button to change the playlist
   */
  dropdown;

  /**
   * The constructor of Playlist, which initializes the attributes
   */
  constructor() {
    this.queue = new Queue(this.collapsePlaylist);
    this.closeButton = document.querySelector(".close");
    this.playlist = document.querySelector(".playlist");
    this.closeButton.addEventListener("click", () => {
      const modalPlaylist = document.querySelector(".playlist");
      modalPlaylist.classList.remove("active");
    });
    this.dropdown = document.getElementById("mon_select");
  }

  /**
   * This function permits to init the playlist, calling the initialization of the header,
   * as well as checking if the user is connected to init the dropdown menu
   * @returns {Promise<void>}
   */
  init = async () => {
    await this.updateHeader();

    let selectedPlaylist = this.dropdown.selectedIndex;
    let listeMusiqueBDD = [];
    let isUserConnected;
    let arrayOfFav;
    let userPlaylists;
    let data = await Utils.getUser();
    if (data == null) {
      isUserConnected = false;
    } else {
      isUserConnected = true;
      arrayOfFav = data.likes;
      userPlaylists = data.playlists;
    }

    if (isUserConnected) {
      userPlaylists.forEach(async (element) => {
        let option = document.createElement("option");
        option.value = element;
        Utils.getPlaylist(element).then((datas) => {
          option.text = datas.nom;
        });
        this.dropdown.add(option);
      });
      let favorite = document.createElement("option");
      favorite.value = "fav";
      favorite.text = "Favoris";
      this.dropdown.add(favorite);
    } else {
      Utils.getListeMusique().then((d) => {
        listeMusiqueBDD = d;
      });
    }

    this.dropdown.addEventListener("change", async (event) => {
      selectedPlaylist =
        this.dropdown.options[this.dropdown.selectedIndex].value;
      listeMusiqueBDD = [];
      var divListe = document.getElementById("liste");
      if (selectedPlaylist === "fav") {
        arrayOfFav.forEach((fav) => {
          Utils.getOneMusic(fav.musique, (res) => {
            listeMusiqueBDD.push(res);
            if (listeMusiqueBDD.length === arrayOfFav.length) {
              divListe.innerHTML = "";
              this.queue.iterateListeItem(listeMusiqueBDD);
            }
          });
        });
      } else {
        const playlist = await Utils.getPlaylist(selectedPlaylist);
        playlist.musiques.forEach((musique) => {
          Utils.getOneMusic(musique.musique, (res) => {
            listeMusiqueBDD.push(res);
            if (listeMusiqueBDD.length === playlist.musiques.length) {
              divListe.innerHTML = "";
              this.queue.iterateListeItem(listeMusiqueBDD);
            }
          });
        });
      }
    });
  };

  /**
   * This function permits to show or hide the playlist html division
   */
  collapsePlaylist = () => {
    this.playlist.classList.add("active");
  };

  /**
   * This initializes the header of the playlist, and update it if necessary
   * @returns {Promise<void>}
   */
  updateHeader = async () => {
    let listMusic;
    await Utils.getListeMusique().then((data) => {
      listMusic = data;
    });

    const nbMusic = listMusic.length;
    document.getElementById("nbMusic").innerHTML = nbMusic + " titres";

    let timeTotal = 0;
    listMusic.forEach((element) => {
      timeTotal += element.duree;
    });
    document.getElementById("timeTotal").innerHTML =
      Utils.secondToMinutes(timeTotal);
  };
}
