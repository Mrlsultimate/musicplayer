/**
 * This class contains some utils functions
 */
export class Utils {
  /**
   * Use to convert second to minutes
   * @param secondToConvert, the value to convert in minutes
   * @returns {string} the value convert in minutes
   */
  static secondToMinutes = (secondToConvert) => {
    let minutes = Math.floor(secondToConvert / 60);
    let seconds = Math.floor(secondToConvert - minutes * 60);
    let hours = Math.floor(secondToConvert / 3600);
    hours = hours === 0 ? "" : hours + ":";
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    return hours + "" + minutes + ":" + seconds;
  };

  /**
   * Request to backend to receive all the musics
   * @returns {Promise<* | undefined>} the json object which contains musics
   */
  static getListeMusique = async () => {
    return fetch("/api/musique/music", {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }).then(async (response) => {
      try {
        return await response.json();
      } catch (err) {
        console.log(err);
      }
    });
  };

  /**
   * Request to backend to receive the user online
   * @returns {Promise<* | undefined>} return the json object which contains the user
   */
  static getUser = async () => {
    return fetch("/api/user/getUser", {
      method: "get",
      credentials: "include",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }).then(async (response) => {
      try {
        return response.json();
      } catch (err) {
        console.log(err);
      }
    });
  };

  static getOneMusic = (id, callback) => {
    fetch("/api/musique/music/" + id, {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then((res) => res.json())
      .then((res) => {
        callback(res);
      });
  };

  static getPlaylist = async (id) => {
    return fetch("/api/playlist/" + id, {
      method: "get",
      credentials: "include",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }).then(async (response) => {
      try {
        return response.json();
      } catch (err) {
        console.log(err);
      }
    });
  };

  /**
   * This function is requesting the api to get the user and know his favorites musics. Then we check if the music in parameter is in this list
   * @param music the music we want to know if it's in favorite
   * @returns {Promise<boolean|*>}
   */
  static isFavoriteMusic = async (music) => {
    try {
      let arrayOfFav;
      let data = await Utils.getUser();
      arrayOfFav = data.likes;
      let isFav = false;

      for (let i = 0; i < arrayOfFav.length; i++) {
        if (arrayOfFav[i].musique === music._id) {
          isFav = true;
          break;
        }
      }

      return isFav;
    } catch (error) {
      return error;
    }
  };
}
